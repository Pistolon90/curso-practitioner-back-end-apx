package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController

// Pone un mapeo genérico a nivel de controlador para empezar por apitechu v2
// No le ponemos la barra a la derecha
@RequestMapping("/apitechu/v2")

public class ProductController {

    // Servicio
    // Repositorio
    // Conexión BD
    // Lance la consulta a la BD
    // Consulta a la BD

    // Autowired es un inyector de dependencias que relaciona las clases
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK //código de estado
        );

        //return new ArrayList<>();
        // Nos devuelve el array vacío en el Postman.
    }

    @PostMapping("/products") //Se añaden productos a través del Postman en el Body
    public ResponseEntity<ProductModel> addProduct (@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto a crear es " + product.getId());
        System.out.println("La descripción del producto a crear es " + product.getDesc());
        System.out.println("El precio del producto a crear es " + product.getPrice());

        // Se crea el nuevo producto pero no se almacena, sólo se displaya en las trazas del idea.
        //return new ProductModel();

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }

    //Obtener producto en base a su id
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
            // ? Ternario, si es TRUE te devuelve la izquierda de : si es FALSE la derecha de :
            result.isPresent() ? result.get() : "Producto no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND //Muestra 404 en Postman
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va a actualizar es " + id);
        System.out.println("La id del producto que se va a actualizar es " + product.getId());
        System.out.println("La descripción del producto que se va a actualizar es " + product.getDesc());
        System.out.println("El precio del producto que se va a actualizar es " + product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("delete Product");
        System.out.println("La id del producto a borrar " + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}

//Nos recomienda el profesor no copiar todo el proyecto entero, copiar por partes.
