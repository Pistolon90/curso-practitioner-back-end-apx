package com.techu.apitechudb.repositories;


import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    public UserModel save(UserModel user) {
        System.out.println("Save en UserRepository");

        ApitechudbApplication.userModels.add(user);

        return user;
    }

    public ArrayList<UserModel> findAll() {
        System.out.println("findAll en UserRepository");

        return ApitechudbApplication.userModels;

        //userModel.getAge() == ageFilter es lo mismo que
        // public filterUserByAge(userModel) {
        // retur userModel.getAge() == ageFilter
    }


    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserRepository");
        System.out.println("La id es " + id);

        Optional<UserModel> result = Optional.empty();

        for (UserModel userIntList : ApitechudbApplication.userModels) {
            if (userIntList.getId().equals(id)) {
                System.out.println("Usuario encontrado con la id " + id);
                result = Optional.of(userIntList);
            }
        }

        return result;
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate = this.findById(user.getId());

        if (userToUpdate.isPresent() == true) {
            System.out.println("Usuario para actualizar encontrado");

            UserModel userFromList = userToUpdate.get();

            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }

        return user;
    }

    public void delete(UserModel user) {
        System.out.println("delete en UserRepository");
        System.out.println("Borrando usuario");

        ApitechudbApplication.userModels.remove(user);
    }
}
