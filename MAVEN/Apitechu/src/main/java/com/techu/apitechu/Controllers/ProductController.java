package com.techu.apitechu.Controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.Models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    // CRUD --  create, read, update, delete
    // HEAD Te devuelve solo la cabecera de la respuesta
    // OPTIONS Te devuelven temas de configuración del servidor

    static final String ApiBaseUrl = "/apitechu/v1";

    @GetMapping(ApiBaseUrl + "/products")

    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel getProductByid(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }

        return result;
    }

    @PostMapping(ApiBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en el parámetro URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel producInlist : ApitechuApplication.productModels) {
            if (producInlist.getId().equals(id)) {
                producInlist.setId(product.getId());
                producInlist.setDesc(product.getDesc());
                producInlist.setPrice(product.getPrice());
            }
        }

        return product;
    }

    //*Hacer Patch para que actualice parcialmente el precio y/o la descripción
    @PatchMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en el parámetro URL es " + id);
        System.out.println("La descripción del producto a actualizar es " + productData.getDesc());
        System.out.println("El precio del producto a actualizar es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = productInList;

                if (productData.getDesc() != null) {
                    System.out.println("Actualizando descripción del producto");
                    productInList.setDesc(productData.getDesc());
                }

                if (productData.getPrice() > 0) {
                    System.out.println("Actualizando precio del producto");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

        return result;
    }

    @DeleteMapping(ApiBaseUrl + "/products/{id}")
    public ProductModel deleteProduct (@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }
        //*Si ha encontrado el elemento entonces lo borra.
        if (foundCompany == true) {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

}

